Oxbotica

one_to_hundred.php    - In the root folder contains the buzz fizz code example.
calculator.feature    - in tests/functional folder contains the automated test cases.
FunctionalTester.php  - _support folder contains the custom functions to support the calculator.feature.

To run the tests:
1. Run local version of webdriver - Open new terminal and run command: Java -jar selenium-server-standalone-3.10.0.jar
2. From root folder use command: vendor/bin/codecept run functional -g testcalc --html=testcalc_results.html
3. Check HTML report generated will - located in _output folder. (When viewed in a browser it will show the passes and errors)
