@calcBulid2
Feature: Basic calculator Build 2

    As a user I wish to test the basic calculator application to find any issues

    @testcalc @add
    Scenario: Using the calculator app with Build 2 selected to add numbers
        Given I open web page "https://testsheepnz.github.io/BasicCalculator.html"
        And I should see text "Basic Calculator"
        And I select option from dropdown "#selectBuild > option:nth-child(3)"
        And I should see "2" option selected in dropdown "#selectBuild"
        When I enter "10" into text box "#number1Field"
        And I enter "10" into text box "#number2Field"
        And I select option from dropdown "#selectOperationDropdown > option:nth-child(1)"
        And I should see "Add" option selected in dropdown "#selectOperationDropdown"
        And I click "#calculateButton"
        Then I should see "20" in field "#numberAnswerField"


    @testcalc @subtract
    Scenario: Using the calculator app with Build 2 selected to subtract numbers
        Given I open web page "https://testsheepnz.github.io/BasicCalculator.html"
        And I should see text "Basic Calculator"
        And I select option from dropdown "#selectBuild > option:nth-child(3)"
        And I should see "2" option selected in dropdown "#selectBuild"
        When I enter "10" into text box "#number1Field"
        And I enter "5" into text box "#number2Field"
        And I select option from dropdown "#selectOperationDropdown > option:nth-child(2)"
        And I should see "Subtract" option selected in dropdown "#selectOperationDropdown"
        And I click "#calculateButton"
        Then I should see "5" in field "#numberAnswerField"


    @testcalc @multiply
    Scenario: Using the calculator app with Build 2 selected to multiply numbers
        Given I open web page "https://testsheepnz.github.io/BasicCalculator.html"
        And I should see text "Basic Calculator"
        And I select option from dropdown "#selectBuild > option:nth-child(3)"
        And I should see "2" option selected in dropdown "#selectBuild"
        When I enter "10" into text box "#number1Field"
        And I enter "5" into text box "#number2Field"
        And I select option from dropdown "#selectOperationDropdown > option:nth-child(3)"
        And I should see "Multiply" option selected in dropdown "#selectOperationDropdown"
        And I click "#calculateButton"
        Then I should see "50" in field "#numberAnswerField"


    @testcalc @divide
    Scenario: Using the calculator app with Build 2 selected to divide numbers
        Given I open web page "https://testsheepnz.github.io/BasicCalculator.html"
        And I should see text "Basic Calculator"
        And I select option from dropdown "#selectBuild > option:nth-child(3)"
        And I should see "2" option selected in dropdown "#selectBuild"
        When I enter "100" into text box "#number1Field"
        And I enter "4" into text box "#number2Field"
        And I select option from dropdown "#selectOperationDropdown > option:nth-child(4)"
        And I should see "Divide" option selected in dropdown "#selectOperationDropdown"
        And I click "#calculateButton"
        Then I should see "25" in field "#numberAnswerField"


    @testcalc @concatenate
    Scenario: Using the calculator app with Build 2 selected to concatenate numbers
        Given I open web page "https://testsheepnz.github.io/BasicCalculator.html"
        And I should see text "Basic Calculator"
        And I select option from dropdown "#selectBuild > option:nth-child(3)"
        And I should see "2" option selected in dropdown "#selectBuild"
        When I enter "100" into text box "#number1Field"
        And I enter "40" into text box "#number2Field"
        And I select option from dropdown "#selectOperationDropdown > option:nth-child(5)"
        And I should see "Concatenate" option selected in dropdown "#selectOperationDropdown"
        And I click "#calculateButton"
        Then I should see "10040" in field "#numberAnswerField"