<?php

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
*/
class FunctionalTester extends \Codeception\Actor
{
    use _generated\FunctionalTesterActions;

    /**
     * Define custom actions here
     */

    
    /**
     * @Given /^I open web page "([^"]*)"$/
     */
   
    public function iAmOnWebPage($url)
    {
        $this->amOnUrl($url);
    }

    /**
     * @Given /^I should see text "([^"]*)"$/
     */
   
    public function iSeeText($text)
    {
        $this->see($text);
    }


    /**
     * @Given /^I select option from dropdown "([^"]*)"$/
     */
   
    public function iSelectFromDropdown($dropdownId)
    {
        $this->click($dropdownId);
    }
    
    /**
      * @Given /^I should see "([^"]*)" option selected in dropdown "([^"]*)"$/
      */
   
    public function iSeeSelectedInDropdown($ddSelected, $dropdownId)
    {
        $this->seeOptionIsSelected($dropdownId, $ddSelected);
    }

    /**
     * @Given /^I enter "([^"]*)" into text box "([^"]*)"$/
     */
   
    public function iEnterTextInField($text, $fieldId)
    {
        $this->fillField($fieldId, $text);
    }

    /**
     * @Given /^I click "([^"]*)"$/
     */
   
    public function iClick($element)
    {
        $this->click($element);
    }

    /**
    * @Given /^I should see "([^"]*)" in field "([^"]*)"$/
    */
  
    public function iSeeResultInField($result, $fieldId)
    {
        $this->seeInField($fieldId, $result);
    }
}
