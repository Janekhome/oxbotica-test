<html>
<body>

<?php
// create range 1 - 100 and save as a variable
foreach (range(1, 100) as $number) {
    // if the number is not divisible by 3 or 5 then print number
    if (0 !== $number % 3 && 0 !== $number % 5) {
        echo $number.'<br>';
        continue;
    }
    // if variable is divisible by 3 replace number with Fizz
    if (0 === $number % 3) {
        echo 'Fizz';
    }
    // if variable is divisible by 5 replace number with Buzz
    if (0 === $number % 5) {
        echo 'Buzz';
    }

    echo '<br>';
}

?>

</body>
</html>
